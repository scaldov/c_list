#ifndef LIST_H
#define LIST_H

//typedef struct c_list_node;

typedef struct c_list_node{
  struct c_list_node *prev;
  struct c_list_node *next;
  void *data;
  int size;
} c_list_node;

typedef struct {
  int size;
  c_list_node begin_node;
  c_list_node end_node;
  c_list_node *begin;
  c_list_node *end;
} c_list;

extern c_list *c_list_new();
extern c_list *c_list_delete(c_list *list);
extern c_list_node *c_list_node_new(void *data, int size);
extern void c_list_node_delete(c_list_node *node, void (dealloc)(void*));
extern int c_list_insert(c_list *list, c_list_node *after, c_list_node *node);
extern int c_list_append(c_list *list, c_list_node *node);
extern int c_list_remove(c_list *list, c_list_node *node);
extern void c_list_clear(c_list *list);

#endif // LIST_H
