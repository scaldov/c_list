#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>

#include "list.h"

c_list *c_list_new() {
    c_list *list = (c_list*)malloc(sizeof(c_list));
    list->begin = &list->begin_node;
    list->end = &list->end_node;
    list->begin->prev = 0;
    list->begin->next = list->end;
    list->end->prev = list->begin;
    list->end->next = 0;
    list->size = 0;
    return list;
}

c_list *c_list_delete(c_list *list) {
    free(list);
}

c_list_node *c_list_node_new(void *data, int size){
    c_list_node *node = (c_list_node*)malloc(sizeof(c_list_node));
    node->next = node->prev = 0;
    if(!data) {
        node->data = malloc(size);
    } else
        node->data = data;
    node->size = size;
    return node;
}

void c_list_node_delete(c_list_node *node, void (dealloc)(void*)){
    if(dealloc) dealloc(node->data);
    free(node);
}

int c_list_insert(c_list *list, c_list_node *after, c_list_node *node) {
    node->prev = after;
    node->next = after->next;
    after->next->prev = node;
    after->next = node;
    list->size ++;
    return list->size;
}

int c_list_append(c_list *list, c_list_node *node) {
    return c_list_insert(list, list->end->prev, node);
}

int c_list_remove(c_list *list, c_list_node *node) {
    node->prev->next = node->next;
    node->next->prev = node->prev;
    list->size --;
    return list->size;
}

void c_list_clear(c_list *list) {
    list->begin = &list->begin_node;
    list->end = &list->end_node;
}


