#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>

#include "list.h"

int main(int argc, char **argv) {
    c_list *list = c_list_new();
    c_list_node *node;
    c_list_node nodes[20];
    // dynamycally allocate node
    node = c_list_node_new((void*)-1, 0);
    node->data = (void*) 1;
    // use preallocated nodes from memory pool
    for(int i = 0; i < sizeof(nodes) / sizeof(c_list_node); i++) nodes[i].data = (void*)(i + 2);
    // append to empty: insert after beginning
    c_list_append(list, node);
    // insert after each other
    c_list_insert(list, node, &nodes[0]);
    c_list_insert(list, &nodes[0], &nodes[1]);
    c_list_insert(list, &nodes[1], &nodes[2]);
    c_list_append(list, &nodes[3]);
    c_list_append(list, &nodes[4]);
    // insert after beginning (before 1 in data)
    c_list_insert(list, list->begin, &nodes[5]);
    c_list_insert(list, list->begin, &nodes[6]);
    c_list_insert(list, list->begin, &nodes[7]);
    // insert after [1] (after 3 in data)
    c_list_insert(list, &nodes[1], &nodes[8]);
    c_list_insert(list, &nodes[1], &nodes[9]);
    c_list_insert(list, &nodes[1], &nodes[10]);
    // expect 9 8 7 1 2 3 12 11 10 4
    // traverse list
    printf("traverse\n");
    for(c_list_node *it = list->begin->next; it != list->end; it = it->next) {
        printf("%d\n", (int)it->data);
    }
    // inverse traverse list
    printf("inverse traverse\n");
    for(c_list_node *it = list->end->prev; it != list->begin; it = it->prev) {
        printf("%d\n", (int)it->data);
    }
    // remove some nodes 6, 7, 9, 10 (8, 9, 11, 12 in data)
    printf("remove\n");
    c_list_remove(list, &nodes[6]);
    c_list_remove(list, &nodes[7]);
    c_list_remove(list, &nodes[9]);
    c_list_remove(list, &nodes[10]);
    // traverse list
    printf("traverse\n");
    for(c_list_node *it = list->begin->next; it != list->end; it = it->next) {
        printf("%d\n", (int)it->data);
    }
    /*
    // remove one by one
    printf("remove one by one\n");
    int l;
    do {
        l = c_list_remove(list, list->begin->next);
    } while(l);
    //*/
    // traverse and remove
    printf("traverse and remove\n");
    //this way
    //for(; list->begin->next != list->end; c_list_remove(list, list->begin->next));
    //or this way
    for(; c_list_remove(list, list->begin->next); );
    // traverse empty list
    printf("traverse empty list\n");
    for(c_list_node *it = list->begin->next; it != list->end; it = it->next) {
        printf("%d\n", (int)it->data);
    }
    c_list_node_delete(node, 0);
    return 0;
}
